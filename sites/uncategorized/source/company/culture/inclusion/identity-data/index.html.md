---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2023-01-31

##### Region Specific Data

| **Region Information** | **Percentages** |
|---|:---:|
| Based in JAPAC | 12.90% |
| Based in EMEA | 30.60% |
| Based in LATAM | 1.29% |
| Based in NORAM | 55.21% |
| **Total Team Members** | **100.00%** |

##### Gender Data

| **Gender (Global)** | **Percentages** |
|---|:---:|
| Men | 63.64% |
| Women | 30.83% |
| Other Gender Identities | 5.53% |
| **Total Team Members** | **100.00%** |

| **Gender in Management (Global)** | **Percentages** |
|---|:---:|
| Men in Management | 60.20% |
| Women in Management | 33.88% |
| Other Gender Identities | 5.92% |
| **Total Team Members** | **100.00%** |

| **Gender in Leadership (Global)** | **Percentages** |
|---|:---:|
| Men in Leadership | 59.26% |
| Women in Leadership | 36.42% |
| Other Gender Identities | 4.32% |
| **Total Team Members** | **100.00%** |

| **Gender in Tech (Global)** | **Percentages** |
|---|:---:|
| Men in Tech | 75.68% |
| Women in Tech | 19.23% |
| Other Gender Identities | 5.09% |
| **Total Team Members** | **100.00%** |

| **Gender in Non-Tech (Global)** | **Percentages** |
|---|:---:|
| Men in Non-Tech | 43.06% |
| Women in Non-Tech | 50.97% |
| Other Gender Identities | 5.98% |
| **Total Team Members** | **100.00%** |

| **Gender in Sales (Global)** | **Percentages** |
|---|:---:|
| Men in Sales | 65.13% |
| Women in Sales | 29.15% |
| Other Gender Identities | 5.72% |
| **Total Team Members** | **100.00%** |

##### Underrepresented Groups (URG) Data**

| **URG (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 80.04% |
| URG | 17.20% |
| Did Not Identify | 2.76% |
| **Total Team Members** | **100.00%** |

| **URG in Tech (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 83.77% |
| URG | 13.91% |
| Did Not Identify | 2.32% |
| **Total Team Members** | **100.00%** |

| **URG in Non-Tech (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 77.62% |
| URG | 20.68% |
| Did Not Identify | 1.70% |
| **Total Team Members** | **100.00%** |

| **URG in Sales (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 79.40% |
| URG | 16.67% |
| Did Not Identify | 3.94% |
| **Total Team Members** | **100.00%** |

| **URG in Management (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 85.14% |
| URG | 13.14% |
| Did Not Identify | 1.71% |
| **Total Team Members** | **100.00%** |

| **URG in Leadership (US Only)** | **Percentages** |
|---|:---:|
| Non-URG | 85.29% |
| URG | 13.24% |
| Did Not Identify | 1.47% |
| **Total Team Members** | **100.00%** |


**Due to data and or legal limitations, this is not an exhaustive list of all of our underrepresented groups.  Those with disabilities, those that identify as LGBTQIA+, etc. who choose not to disclose or underrepresented ethnicities outside of the US. 

The DIB Team is actively working on finding data sets outside the US and inclusion metrics for underrepresented groups we cannot report on as team member representation. 

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's People Analytics Team, WorkDay
